const { MessageEmbed } = require("discord.js")
const db = require("../../db.json")
var moment = require('moment')

module.exports = (client,guild) => {
	const guildsjoinleaveChannel = client.channels.cache.get(db["settings"]["guildsjoinleave"])
	if(!guildsjoinleaveChannel) return
	let verifLevels = {
		"NONE": "Aucun",
		"LOW": "Faible",
		"MEDIUM": "Moyen",
		"HIGH": "Élevé",
		"VERY_HIGH": "Maximum"
	}
	let regions = {
		"europe":"Europe 🇪🇺",
		"brazil":"Brésil 🇧🇷",
		"hongkong":"Hong Kong 🇭🇰",
		"india":"Inde 🇮🇳",
		"japan":"Japon 🇯🇵",
		"russia":"Russie 🇷🇺",
		"singapore":"Singapour 🇸🇬",
		"southafrica": "Afrique du Sud 🇿🇦",
		"sydney":"Sydney 🇦🇺",
		"us-central":"États-Unis - Centre 🇺🇸",
		"us-east":"États-Unis - Est 🇺🇸",
		"us-south":"États-Unis - Sud 🇺🇸",
		"us-west":"États-Unis - Ouest 🇺🇸"
	}
	guildsjoinleaveChannel.send(new MessageEmbed()
		.setColor('GREEN')
		.setTitle(guild.name)
		.setThumbnail(guild.iconURL())
		.addField('🌎  Région',regions[guild.region],true)
		.addField('⚠️  Niveau de vérification',verifLevels[guild.verificationLevel],true)
		.addField("\u200B","\u200B",true)
		.addField('👑  Propriétaire',guild.owner,true)
		.addField('📅  Date de création',moment(guild.createdAt).format("DD/MM/YYYY à HH:mm:ss"),true)
		.addField("\u200B","\u200B",true)
		.addField('👫  Utilisateurs',guild.members.cache.filter(member => !member.user.bot).size,true)
		.addField('👾  Bots',guild.members.cache.filter(member => member.user.bot).size,true)
		.addField('👤  Membres',guild.memberCount,true)
		.setTimestamp()
		.setFooter(`ID: ${guild.id}`)
	)
}