const { writeFileSync } = require("fs")
const db = require("../../db.json")

module.exports = (client,guild) => {
	db["guilds"].push({guild:guild.id})
	writeFileSync("./db.json",JSON.stringify(db,null,4))
}
