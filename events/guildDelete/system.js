const { writeFileSync } = require("fs")
const db = require("../../db.json")

module.exports = (client,guild) => {
	db["guilds"].splice(db["guilds"].indexOf(db["guilds"].find(d => d.guild === guild.id)),1)
    writeFileSync("./db.json",JSON.stringify(db,null,4))
}
