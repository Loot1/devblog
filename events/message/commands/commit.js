const { MessageEmbed } = require('discord.js')
const { writeFileSync } = require("fs")
const db = require("../../../db.json")

function msg(message,color,title,desc) {
    message.channel.send(new MessageEmbed()
    .setColor(color)
    .setTitle(title)
    .setDescription(desc)
    .setTimestamp()
    .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
	)
}

module.exports = {
    name: 'commit',
    aliases: ['commit'],
    description: 'Définir ou changer le titre',
    cooldown: 3,
    permissions: {
        client: [],
        user: ['ADMINISTRATOR'],
        owneronly: false
    },
    arguments : 1,
    usage: '<texte>',
	execute(client,message,args) {
        const data = db["guilds"].find(d => d.guild === message.guild.id)
        const define = data.title ? false : true
        const text = args.join(" ")
        data.title = text
        writeFileSync("./db.json",JSON.stringify(db,null,4))
        msg(message,'BLUE',`🏷️  Titre ${define ? "défini" : "changé"}`,`\`${text}\``)
    }
}