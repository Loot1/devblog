const { MessageEmbed } = require('discord.js')
const db = require("../../../db.json")

module.exports = {
    name: 'help',
    aliases: ['help'],
    description: 'Visualisation des commandes',
    cooldown: 3,
    permissions: {
        client: [],
        user: [],
        owneronly: false
    },
    arguments : 0,
    usage: '',
    async execute(client,message,args) {
        const invite = await client.channels.cache.get(db["settings"]["guildsjoinleave"]).createInvite({maxAge:0})
        const embed = new MessageEmbed()
        .setColor('BLUE')
        .setTitle('❔  Aide')
        .setDescription(`[Support](${invite} 'Clique pour rejoindre le serveur de support du bot')`)
        .setTimestamp()
        .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
        client.commands.filter((cmd) => (cmd.name != "help") && (cmd.permissions.owneronly === false)).forEach((cmd) => embed.addField(`\`${cmd.name}\``,cmd.description,true))
        message.channel.send(embed)
    }
}