const { MessageEmbed } = require('discord.js')
const db = require("../../../db.json")

function msg(message,color,title,desc) {
    message.channel.send(new MessageEmbed()
    .setColor(color)
    .setTitle(title)
    .setDescription(desc)
    .setTimestamp()
    .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
	)
}

module.exports = {
    name: 'preview',
    aliases: ['preview'],
    description: 'Avoir un aperçu de la mise à jour',
    cooldown: 3,
    permissions: {
        client: [],
        user: ['ADMINISTRATOR'],
        owneronly: false
    },
    arguments : 1,
    usage: '<salon>',
	execute(client,message,args) {
        const data = db["guilds"].find(d => d.guild === message.guild.id)
        if(message.mentions.channels.size > 0) {
            if(message.mentions.channels.size === 1) {
                if(args.join(" ").startsWith(`<#${message.mentions.channels.first().id}>`)) var channel = message.mentions.channels.first()
                else return msg(message,'RED',"#️⃣  Salon indéfini","La mention n'est pas le premier argument entré.",true)
            } else return msg(message,'RED',"#️⃣  Salon indéfini","Il y a plusieurs mentions de salon dans ce message.",true)
        }
        if(!channel) return msg(message,'RED',"#️⃣  Salon indéfini","Le salon n'est pas défini. Vous devez le mentionner pour le définir.",true)
        if(channel.type !== "text") return msg(message,'RED',"#️⃣  Salon indéfini","Le salon doit impérativement être un salon textuel, pas un salon vocal, une catégorie ou encore un salon d'annonce. ",true)
        channel.send(new MessageEmbed()
        .setColor('GREEN')
        .setTitle(data.commit ? data.commit : "Mise à jour")
        .setDescription(`✅ Ajout(s) :\n${data.add.join("\n")}\n✏️ Changement(s) :\n${data.change.join("\n")}\n❌ Suppression(s) :\n${data.remove.join("\n")}`)
        .setTimestamp()
        .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
        )
    }
}