const { writeFileSync } = require("fs")
const db = require("../../db.json")

module.exports = (client) => {

	console.info('Devblog')

	client.guilds.cache.filter(g => !db["guilds"].find(d => d.guild === g.id)).forEach((g) => db["guilds"].push({guild:g.id}))
	db["guilds"].filter(d => !client.guilds.cache.get(d.guild)).forEach((d) => db["guilds"].splice(db["guilds"].indexOf(d),1))
	writeFileSync("./db.json",JSON.stringify(db,null,4))

}